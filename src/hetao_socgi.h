/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#ifndef _H_HETAO_SOCGI_
#define _H_HETAO_SOCGI_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "fasterhttp.h"

#if ( defined _WIN32 )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		_declspec(dllexport)
#endif
#elif ( defined __unix ) || ( defined __linux__ )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		extern
#endif
#endif

/*
 * error code
 **/

#define SOCGI_FATAL_FASTERHTTP				-101

/*
 * util
 **/

#define STRNCMPSTRN(_str1_,_str1_len_,_cmp_,_str2_,_str2_len_)	( (_str1_len_) _cmp_ (_str2_len_) && STRNCMP( (_str1_) , _cmp_ , (_str2_) , (_str2_len_) ) )
#define STRNEQSTR(_str1_,_str1_len_,_str2_)		STRNCMPSTRN( (_str1_) , (_str1_len_) , == , (_str2_) , (strlen(_str2_)) )
#define STRNEQRSTR(_str1_,_str1_len_,_literal_str2_)	STRNCMPSTRN( (_str1_) , (_str1_len_) , == , (_literal_str2_) , (sizeof(_literal_str2_)-1) )

#ifndef HTTP_RETURN_NEWLINE
#define HTTP_RETURN_NEWLINE	"\r\n"
#endif

#define HTML_NEWLINE		"<br />"
#define HTML_RETURN_NEWLINE	"<p />"

#ifndef SNPRINTF
#if ( defined _WIN32 )
#define SNPRINTF        _snprintf
#define VSNPRINTF       _vsnprintf
#elif ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ )
#define SNPRINTF        snprintf
#define VSNPRINTF       vsnprintf
#endif
#endif

#define BUFNPRINTF(_buf_base_,_buf_size_,_str_len_,_format_,...)	\
	{ \
		int	len ; \
		len = SNPRINTF( (_buf_base_)+(_str_len_) , (_buf_size_) , (_format_) , __VA_ARGS__ ) ; \
		if( len > 0 ) \
			(_str_len_) += len ; \
	} \

#define BUFPRINTF(_buf_base_,_str_len_,_format_,...)		BUFNPRINTF(_buf_base_,sizeof(_buf_base_),_str_len_,_format_,__VA_ARGS__)

#define BUFNSTRCAT(_buf_base_,_buf_size_,_str_len_,_cat_str_)	\
	{ \
		int	len ; \
		len = SNPRINTF( (_buf_base_)+(_str_len_) , (_buf_size_) , (_cat_str_) ) ; \
		if( len > 0 ) \
			(_str_len_) += len ; \
	} \

#define BUFSTRCAT(_buf_base_,_str_len_,_cat_str_)		BUFNSTRCAT(_buf_base_,sizeof(_buf_base_),_str_len_,_cat_str_)

/*
 * http application context
 **/

struct HttpApplicationContext ;

typedef int funcInitHttpApplication( struct HttpApplicationContext *ctx );
typedef int funcRedirectHttpDomain( struct HttpApplicationContext *ctx );
typedef int funcRewriteHttpUri( struct HttpApplicationContext *ctx );
typedef int funcBeforeProcessHttpResource( struct HttpApplicationContext *ctx );
typedef int funcProcessHttpResource( struct HttpApplicationContext *ctx );
typedef int funcSelectForwardServer( struct HttpApplicationContext *ctx );
typedef int funcCallHttpApplication( struct HttpApplicationContext *ctx );
typedef int funcGetHttpResource( struct HttpApplicationContext *ctx );
typedef int funcAfterProcessHttpResource( struct HttpApplicationContext *ctx );
typedef int funcCleanHttpApplication( struct HttpApplicationContext *ctx );

#define INITHTTPAPPLICATION		_WINDLL_FUNC funcInitHttpApplication
#define REDIRECTHTTPDOMAIN		_WINDLL_FUNC funcRedirectHttpDomain
#define REWRITEHTTPURI			_WINDLL_FUNC funcRewriteHttpUri
#define BEFOREPROCESSHTTPRESOURCE	_WINDLL_FUNC funcBeforeProcessHttpResource
#define PROCESSHTTPRESOURCE		_WINDLL_FUNC funcProcessHttpResource
#define SELECTFORWARDSERVER		_WINDLL_FUNC funcSelectForwardServer
#define CALLHTTPAPPLICATION		_WINDLL_FUNC funcCallHttpApplication
#define GETHTTPRESOURCE			_WINDLL_FUNC funcGetHttpResource
#define AFTERPROCESSHTTPRESOURCE	_WINDLL_FUNC funcAfterProcessHttpResource
#define CLEANHTTPAPPLICATION		_WINDLL_FUNC funcCleanHttpApplication

_WINDLL_FUNC void SOCGISetUserData( struct HttpApplicationContext *ctx , void *user_data );
_WINDLL_FUNC void *SOCGIGetUserData( struct HttpApplicationContext *ctx );

_WINDLL_FUNC char *SOCGIGetCurrentPathname( struct HttpApplicationContext *ctx );
_WINDLL_FUNC char *SOCGIGetConfigPathfilename( struct HttpApplicationContext *ctx );

_WINDLL_FUNC char *SOCGIGetHttpHeaderPtr_METHOD( struct HttpApplicationContext *ctx , int *p_value_len );
_WINDLL_FUNC char *SOCGIGetHttpHeaderPtr_URI( struct HttpApplicationContext *ctx , int *p_value_len );
_WINDLL_FUNC char *SOCGIGetHttpHeaderPtr_VERSION( struct HttpApplicationContext *ctx , int *p_value_len );
_WINDLL_FUNC char *SOCGIQueryHttpHeaderPtr( struct HttpApplicationContext *ctx , char *name , int *p_value_len );
_WINDLL_FUNC struct HttpHeader *SOCGITravelHttpHeaderPtr( struct HttpApplicationContext *ctx , struct HttpHeader *p_header );
_WINDLL_FUNC char *SOCGIGetHttpHeaderNamePtr( struct HttpHeader *p_header , int *p_name_len );
_WINDLL_FUNC char *SOCGIGetHttpHeaderValuePtr( struct HttpHeader *p_header , int *p_value_len );
_WINDLL_FUNC char *SOCGIGetHttpBodyPtr( struct HttpApplicationContext *ctx , int *p_body_len );

struct HttpEnv ;

_WINDLL_FUNC int SOCGIFormatHttpResponseV( struct HttpEnv *http , char *http_response_body , int http_response_body_len , char *http_header_format , va_list valist );
_WINDLL_FUNC int SOCGIFormatHttpResponse( struct HttpApplicationContext *ctx , char *http_response_body , int http_response_body_len , char *http_header_format , ... );

#endif

